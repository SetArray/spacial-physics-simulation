package space.main;

import static org.lwjgl.opengl.GL11.*;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.opengl.ImageIOImageData;

import space.data.FileHandler;
import space.data.Textures;
import space.game.Game;
import space.game.entity.TestPlanet;
import space.util.system.MouseUtil;
import space.util.system.Timer;

public class Main
{
	public static Main instance;
	public GameState state;
	private Timer timer;
	public Game game;

	public void start()
	{
		init();
		mainLoop();
	}

	public void mainLoop()
	{	
		while (!Display.isCloseRequested())
		{
			if (state == GameState.INTRO)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				
				state = GameState.PREINIT;
			}
			
			if(state == GameState.PREINIT)
			{
				state = GameState.MAIN_MENU;
			}

			if (state == GameState.MAIN_MENU)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				
				state = GameState.GAME;
				game = new Game();
				
			}

			if (state == GameState.GAME)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				
				game.update();
			}

			update();
		}

		dispose();
	}

	public void update()
	{
		timer.updateFPS();
		Display.setTitle("Space ~ FPS: " + timer.getFPS());
		
		Display.update();
		Display.sync(120);
	}

	public void init()
	{
		initWindow();
		initDisplay();
		initObjects();

		timer.getDelta();
	}

	private void initDisplay()
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
	}

	private void initObjects()
	{
		instance = this;
		state = GameState.INTRO;
		timer = new Timer();
		
		Textures.loadTextures();
	}

	private void initWindow()
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(896, 768));
			Display.setTitle("Space ~ FPS: ");
			Display.setIcon(new ByteBuffer[] {new ImageIOImageData().imageToByteBuffer(ImageIO.read(FileHandler.getFile("icons/icon_16.png")), false, false, null)});
			Display.create();
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void dispose()
	{
		Display.destroy();
		System.exit(0);
	}

	public static void main(String[] args)
	{
		Main main = new Main();
		main.start();
	}
}
