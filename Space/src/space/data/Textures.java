package space.data;

import org.newdawn.slick.opengl.Texture;

public class Textures
{
	public static Texture TEST_PLANET;
	public static Texture MOON;
	public static Texture PLAYER_0;
	
	public static void loadTextures()
	{
		TEST_PLANET = FileHandler.loadTexture("entities/planets/planet_green_1");
		MOON = FileHandler.loadTexture("entities/planets/moon");
		PLAYER_0 = FileHandler.loadTexture("entities/player/player_0");
	}
}
