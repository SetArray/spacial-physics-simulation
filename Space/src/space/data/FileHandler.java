package space.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class FileHandler
{
	public static Texture loadTexture(String key)
	{
		try
		{
			return TextureLoader.getTexture("png", getFile(key + ".png"));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static InputStream getFile(String file)
	{
		return FileHandler.class.getClassLoader().getResourceAsStream(file);
	}
}
