package space.game;

import space.game.entity.EntityPlayer;
import space.game.entity.Moon;
import space.game.entity.TestPlanet;
import space.util.system.PhysicsUtil;

public class Game
{
	private EntityPlayer player;
	private GameInputHandler input;
	TestPlanet planet;
	private Moon moon;
	
	public Game()
	{
		input = new GameInputHandler(this);
		player = new EntityPlayer(438.63125, 0.0, 90.0, 0.0, 0.0, 0.0);
		
		planet = new TestPlanet(0, 0.0, 0.0, 0.0, 0.0, 0.1);
		moon = new Moon(24463.63125 / 16, 0, 0, 0, 0, 0, 0.05);
		
		player.setVelocityY(PhysicsUtil.calculateOrbitalVelocity(planet, player, PhysicsUtil.getDistanceBetween(planet, player)));
		moon.setVelocityY(PhysicsUtil.calculateOrbitalVelocity(planet, moon, PhysicsUtil.getDistanceBetween(planet, moon)));
	}
	
	public void update()
	{	
		input.update();

		planet.update();
		moon.update();
		
		player.update();
		
		render();
	}
	
	private void render()
	{
		renderBackground();
		renderEntities();
		renderHUD();
	}
	
	private void renderBackground()
	{
		/*glPushMatrix();

		Color.red.bind();
		glTranslated(AbstractEntity.getUniversalTransX(), AbstractEntity.getUniversalTransY(), 0);
		
		glBegin(GL_QUADS);
		{
			glVertex2d(planet.getGridX(), planet.getGridY() + 2);

			glVertex2d(player.getGridX(), player.getGridY() + 2);

			glVertex2d(player.getGridX() + 1, player.getGridY());

			glVertex2d(planet.getGridX() + 1, planet.getGridY());
		}
		glEnd();
		
		glBegin(GL_QUADS);
		{
			glVertex2d(planet.getGridX() - 1000, planet.getGridY());
			glVertex2d(planet.getGridX() - 1000, planet.getGridY() + 2);
			glVertex2d(planet.getGridX() + 1000, planet.getGridY() + 5);
			glVertex2d(planet.getGridX() + 1000, planet.getGridY());
		}
		glEnd();
		glPopMatrix();
		Color.white.bind();*/
	}
	
	private void renderEntities()
	{
		
	}
	
	private void renderHUD()
	{
		
	}
	
	public EntityPlayer getPlayer()
	{
		return player;
	}
}
