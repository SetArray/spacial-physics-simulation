package space.game.entity;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotated;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Rectangle;
import java.util.ArrayList;

import org.newdawn.slick.opengl.Texture;

import space.util.system.MathUtil;
import space.util.system.PhysicsUtil;

public abstract class AbstractEntity implements IEntity
{
	protected static double universalTransX = 0;
	protected static double universalTransY = 0;

	protected double gridx, gridy;
	protected double rot;

	/**
	 * Velocity in m/s
	 */
	protected double velocityX, velocityY, velocityRot;

	protected double mass, density, volume, gravPull;
	protected double pullRadius;

	protected double temperature;

	protected Rectangle bounds;
	protected Texture texture;

	public AbstractEntity(double gridx, double gridy, double rot, double velocityX, double velocityY,
			double velocityRot, double mass, Rectangle bounds, Texture texture)
	{
		this.gridx = gridx;
		this.gridy = gridy;
		this.rot = rot;
		this.velocityRot = velocityRot;
		this.velocityX = velocityX;
		this.velocityY = velocityY;
		this.mass = mass;
		this.bounds = bounds;
		this.texture = texture;

		Objects.addEntity(this);
		// a = G*M/r^2
	}

	public AbstractEntity(double gridx, double gridy, Rectangle bounds)
	{
		this.gridx = gridx;
		this.gridy = gridy;
		this.bounds = bounds;
	}

	private ArrayList<IEntity> ents;

	@Override
	public void update()
	{
		gridx += (velocityX / PhysicsUtil.metersPerPixel);
		gridy += (velocityY / PhysicsUtil.metersPerPixel);
		rot += velocityRot;

		ents = Objects.getAllEntities();

		double grav_pull, angle;
		IEntity entity;

		for (int i = 0; i < ents.size(); i++)
		{
			entity = ents.get(i);

			if (entity != this)
			{
				grav_pull = PhysicsUtil.getGravitationalPull(this, entity);
				angle = -Math.atan2(entity.getGridY() - getGridY(), entity.getGridX() - getGridX()) * (180 / Math.PI);
				if (angle < 0)
					angle += 360;

				double deltaX = (entity.getGridX() - getGridX()) * PhysicsUtil.metersPerPixel;
				double deltaY = (entity.getGridY() - getGridY()) * PhysicsUtil.metersPerPixel;

				if (deltaX < 0.0)
				{
					entity.accelerateX(Math.cos(Math.toRadians(angle)) * -grav_pull);
				}
				if (deltaX > 0.0)
				{
					entity.accelerateX(Math.cos(Math.toRadians(angle)) * -grav_pull);
				}

				if (deltaY < 0.0)
				{
					entity.accelerateY(Math.sin(Math.toRadians(angle)) * -grav_pull);
				}
				if (deltaY > 0.0)
				{
					entity.accelerateY(Math.sin(Math.toRadians(angle)) * -grav_pull);
				}
			}
		}

		draw();
	}

	@Override
	public void draw()
	{
		glPushMatrix();

		glTranslated(universalTransX + gridx, universalTransY + gridy, 0);
		glRotated(rot, 0, 0, 1);
		glTranslated(-(universalTransX + gridx), -(universalTransY + gridy), 0);

		glBindTexture(GL_TEXTURE_2D, texture.getTextureID());
		glTranslated(universalTransX, universalTransY, 0);
		glBegin(GL_QUADS);
		{
			glTexCoord2d(0, 0);
			glVertex2d(gridx - (bounds.getWidth() / 2), gridy - (bounds.getHeight() / 2));

			glTexCoord2d(0, 1);
			glVertex2d(gridx - (bounds.getWidth() / 2), gridy + (bounds.getHeight() / 2));

			glTexCoord2d(1, 1);
			glVertex2d(gridx + (bounds.getWidth() / 2), gridy + (bounds.getHeight() / 2));

			glTexCoord2d(1, 0);
			glVertex2d(gridx + (bounds.getWidth() / 2), gridy - (bounds.getHeight() / 2));
		}
		glEnd();
		glPopMatrix();

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	@Override
	public double getGridX()
	{
		return gridx;
	}

	@Override
	public double getGridY()
	{
		return gridy;
	}

	@Override
	public void setRotation(double rot)
	{
		if (rot < 0.0)
			rot += 360.0;
		else if (rot > 360.0)
			rot -= 360.0;

		this.rot = rot;
	}

	@Override
	public double getRotation()
	{
		return rot;
	}

	@Override
	public Rectangle getBounds()
	{
		return null;
	}

	/**
	 * Returns velocity on y-axis in m/s
	 * 
	 * @return
	 */
	@Override
	public double getVelocityY()
	{
		return velocityY;
	}

	/**
	 * Returns velocity on x-axis in m/s
	 * 
	 * @return
	 */
	@Override
	public double getVelocityX()
	{
		return velocityX;
	}

	@Override
	public double getVelocityRot()
	{
		return velocityRot;
	}

	@Override
	public double getMass()
	{
		return mass;
	}

	@Override
	public double getGravitationalPull()
	{
		return 0;
	}

	@Override
	public double getGravitationalRadius()
	{
		return 0;
	}

	@Override
	public double getTemperature()
	{
		return temperature;
	}

	@Override
	public double getDensity()
	{
		return density;
	}

	@Override
	public double getVolume()
	{
		return volume;
	}

	@Override
	public Texture getTexture()
	{
		return texture;
	}

	public static double getUniversalTransX()
	{
		return universalTransX;
	}

	public static double getUniversalTransY()
	{
		return universalTransY;
	}

	@Override
	public void setGridX(double gx)
	{
		this.gridx = gx;
	}

	@Override
	public void setGridY(double gy)
	{
		this.gridy = gy;
	}

	@Override
	public void setVelocityX(double vx)
	{
		this.velocityX = vx;
	}

	@Override
	public void setVelocityY(double vy)
	{
		this.velocityY = vy;
	}

	@Override
	public void setVelocityRot(double vr)
	{
		this.velocityRot = vr;
	}

	@Override
	public void setMass(double mass)
	{
		this.mass = mass;
	}

	@Override
	public void setTemperature(double temp)
	{
		this.temperature = temp;
	}

	@Override
	public void setTexture(Texture texture)
	{
		this.texture = texture;
	}

	public void rotate(double rotation)
	{
		glTranslated(-1 * (-gridx - (bounds.getWidth() / 2)), -1 * (-gridy - (bounds.getHeight() / 2)), 0);
		glRotated(rotation, 0, 0, 1);
		glTranslated(1 * (-gridx - (bounds.getWidth() / 2)), 1 * (-gridy - (bounds.getHeight() / 2)), 0);
	}

	@Override
	public void accelerateX(double x)
	{
		velocityX += x;
	}

	@Override
	public void accelerateY(double y)
	{
		velocityY -= y;
	}

	public static double getGridXToScreenX(double x)
	{
		return (x + universalTransX);
	}

	public static double getGridYToScreenY(double y)
	{
		return (y + universalTransY);
	}

	public static double getScreenXToGridX(double x)
	{
		return (x - universalTransX);
	}

	public static double getScreenYToGridY(double y)
	{
		return (y - universalTransX);
	}
	
	public boolean intersects(IEntity entity)
	{
		return false;
	}
}
