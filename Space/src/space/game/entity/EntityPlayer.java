package space.game.entity;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotated;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Rectangle;

import org.lwjgl.opengl.Display;

import space.data.Textures;
import space.util.system.MathUtil;

public class EntityPlayer extends AbstractEntity
{
	private double thrust, rotThrust;
	
	private double screenx = Display.getWidth() / 2;
	private double screeny = Display.getHeight() / 2;

	public EntityPlayer(double gridx, double gridy, double rot, double velocityX, double velocityY, double velocityRot)
	{
		super(gridx, gridy, rot, velocityX, velocityY, velocityRot, 599000, new Rectangle(64, 64), Textures.PLAYER_0);
		rotThrust = 0.015;
		thrust = 29.4;
	}

	@Override
	public void update()
	{
		super.update();
		
		universalTransX = -gridx + (Display.getWidth() / 2);
		universalTransY = -gridy + (Display.getHeight() / 2);
		
		new PreviousLocation(gridx, gridy);
		PreviousLocation.drawList();
	}
	
	@Override
	public void draw()
	{
		glPushMatrix();

		glTranslated(screenx, screeny, 0);
		glRotated(rot, 0, 0, 1); 
		glTranslated(-screenx, -screeny, 0);
		
		glBindTexture(GL_TEXTURE_2D, texture.getTextureID());
		glBegin(GL_QUADS);
		{
			glTexCoord2d(0, 0);
			glVertex2d(screenx - (bounds.getWidth() / 2), screeny - (bounds.getHeight() / 2));

			glTexCoord2d(0, 1);
			glVertex2d(screenx - (bounds.getWidth() / 2), screeny + (bounds.getHeight() / 2));

			glTexCoord2d(1, 1);
			glVertex2d(screenx + (bounds.getWidth() / 2), screeny + (bounds.getHeight() / 2));

			glTexCoord2d(1, 0);
			glVertex2d(screenx + (bounds.getWidth() / 2), screeny - (bounds.getHeight() / 2));
		}
		glEnd();
		glPopMatrix();
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public void setThrust(double thrust)
	{
		this.thrust = thrust;
	}
	
	public void setRotThrust(double rotThrust)
	{
		this.rotThrust = rotThrust;
	}
	
	public double getThrust()
	{
		return thrust;
	}
	
	public double getRotThrust()
	{
		return rotThrust;
	}
}
