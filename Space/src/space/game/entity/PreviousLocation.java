package space.game.entity;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Rectangle;
import java.util.ArrayList;

import org.newdawn.slick.Color;


public class PreviousLocation extends AbstractEntity
{
	public static ArrayList<PreviousLocation> list = new ArrayList<PreviousLocation>();
	
	private static int count = 0;

	public PreviousLocation(double gridx, double gridy)
	{
		super(gridx, gridy, new Rectangle(3, 3));
		if(list.size() > 300) list.remove(0);
		
		count++;
		
		if(count % 50 == 0)
			list.add(this);
		
		if(count >= 1000) count = 0;
	}

	@Override
	public void update()
	{
		draw();
	}
	
	@Override
	public void draw()
	{
		glPushMatrix();
		Color.red.bind();
		glTranslated(universalTransX, universalTransY, 0);;
		glBegin(GL_QUADS);
		{
			glTexCoord2d(0, 0);
			glVertex2d(gridx - (bounds.getWidth() / 2), gridy - (bounds.getHeight() / 2));

			glTexCoord2d(0, 1);
			glVertex2d(gridx - (bounds.getWidth() / 2), gridy + (bounds.getHeight() / 2));

			glTexCoord2d(1, 1);
			glVertex2d(gridx + (bounds.getWidth() / 2), gridy + (bounds.getHeight() / 2));

			glTexCoord2d(1, 0);
			glVertex2d(gridx + (bounds.getWidth() / 2), gridy - (bounds.getHeight() / 2));
		}
		glEnd();
		Color.white.bind();
		glPopMatrix();
	}
	
	public static void drawList()
	{
		for(int i = 0; i < list.size(); i++)
		{
			list.get(i).update();
		}
	}
}
