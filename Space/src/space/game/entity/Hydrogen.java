package space.game.entity;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotated;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Rectangle;

import org.newdawn.slick.Color;

public class Hydrogen extends AbstractEntity
{

	public Hydrogen(double gridx, double gridy)
	{
		super(gridx, gridy, 0, 0, 0, 0, 1.3782472e-25, new Rectangle(1, 1), null);
	}

	@Override
	public void draw()
	{
		glPushMatrix();

		glTranslated(universalTransX + gridx, universalTransY + gridy, 0);
		glRotated(rot, 0, 0, 1);
		glTranslated(-(universalTransX + gridx), -(universalTransY + gridy), 0);

		Color.cyan.bind();
		
		glTranslated(universalTransX, universalTransY, 0);
		glBegin(GL_QUADS);
		{
			glTexCoord2d(0, 0);
			glVertex2d(gridx - (bounds.getWidth() / 2), gridy - (bounds.getHeight() / 2));

			glTexCoord2d(0, 1);
			glVertex2d(gridx - (bounds.getWidth() / 2), gridy + (bounds.getHeight() / 2));

			glTexCoord2d(1, 1);
			glVertex2d(gridx + (bounds.getWidth() / 2), gridy + (bounds.getHeight() / 2));

			glTexCoord2d(1, 0);
			glVertex2d(gridx + (bounds.getWidth() / 2), gridy - (bounds.getHeight() / 2));
		}
		glEnd();
		glPopMatrix();

		Color.white.bind();
	}

}
