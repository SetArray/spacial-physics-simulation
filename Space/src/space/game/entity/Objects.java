package space.game.entity;

import java.util.ArrayList;

public class Objects
{
	private static ArrayList<IEntity> entities = new ArrayList<IEntity>();
	
	public static ArrayList<IEntity> getAllEntities()
	{
		return entities;
	}
	
	public static void addEntity(IEntity entity)
	{
		entities.add(entity);
	}
	
	public static IEntity getEntity(int index)
	{
		return entities.get(index);
	}
	
	public static boolean entityExists(IEntity entity)
	{
		return entities.contains(entity);
	}
	
	public static int getNumberOfEntities()
	{
		return entities.size();
	}
}
