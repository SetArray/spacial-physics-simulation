package space.game.entity;

import java.awt.Rectangle;

import space.data.Textures;

public class TestPlanet extends AbstractEntity
{

	public TestPlanet(double gridx, double gridy, double rot, double velocityX, double velocityY, double velocityRot)
	{
		super(gridx, gridy, rot, velocityX, velocityY, velocityRot, 5.98e24, new Rectangle(797, 797), Textures.TEST_PLANET);
	}

	@Override
	public void update()
	{
		super.update();
	}
}
