package space.game.entity;

import java.awt.Rectangle;

import space.data.Textures;

public class Moon extends AbstractEntity
{

	public Moon(double gridx, double gridy, double rot, double velocityX, double velocityY, double velocityRot,
			double mass)
	{
		super(gridx, gridy, rot, velocityX, velocityY, velocityRot, 7.34767309e22, new Rectangle(217, 217), Textures.MOON);
	}

}
