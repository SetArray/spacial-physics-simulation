package space.game.entity;

import java.awt.Rectangle;

import org.newdawn.slick.opengl.Texture;

public interface IEntity
{
	public void update();
	public void draw();

	public void setGridX(double gx);
	public void setGridY(double gy);
	public double getGridX();
	public double getGridY();
	
	public void setRotation(double rot);
	public double getRotation();
	
	public Rectangle getBounds();

	public void setVelocityX(double vx);
	public void setVelocityY(double vy);
	public void setVelocityRot(double vr);
	public double getVelocityX();
	public double getVelocityY();
	public double getVelocityRot();
	
	public void accelerateX(double x);
	public void accelerateY(double y);
	
	public void setMass(double mass);
	public double getMass();
	
	public double getGravitationalPull();
	public double getGravitationalRadius();
	
	public void setTemperature(double temp);
	public double getTemperature();
	
	public double getDensity();
	public double getVolume();
	
	public void setTexture(Texture texture);
	public Texture getTexture();
	
	public boolean intersects(IEntity entity);
}
