package space.game;

import static org.lwjgl.opengl.GL11.glScaled;
import static org.lwjgl.opengl.GL11.glTranslated;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import space.game.entity.EntityPlayer;
import space.util.system.MouseUtil;

public class GameInputHandler
{
	Game game;
	EntityPlayer player;
	private double scroll;
	
	public GameInputHandler(Game game)
	{
		this.game = game;
		this.player = game.getPlayer();
	}
	
	public void update()
	{
		this.player = game.getPlayer();
		updateMouse();
		updateKeyboard();
	}
	
	public void updateMouse()
	{
		scroll = MouseUtil.getMouseScroll();
		
		if(scroll != 0.0)
		{
			glTranslated(Display.getWidth() / 2, Display.getHeight() / 2, 0);
			if(scroll > 0)
			{
				glScaled(Math.exp(0.25), Math.exp(0.25), 0);
			}
			if(scroll < 0)
			{
				glScaled(Math.exp(-0.25), Math.exp(-0.25), 0);
			}
			glTranslated(-Display.getWidth() / 2, -Display.getHeight() / 2, 0);
		}
	}
	
	public void updateKeyboard()
	{
		if(Keyboard.isKeyDown(Keyboard.KEY_W))
		{	
			if(player.getRotation() != 0 & player.getRotation() != 180)
				player.accelerateX(Math.sin(Math.toRadians(player.getRotation())) * player.getThrust());
			
			if(player.getRotation() != 90 & player.getRotation() != 270)
				player.accelerateY(Math.cos(Math.toRadians(player.getRotation())) * player.getThrust());
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_A))
			player.setVelocityRot(player.getVelocityRot() - player.getRotThrust());
		if(Keyboard.isKeyDown(Keyboard.KEY_D))
			player.setVelocityRot(player.getVelocityRot() + player.getRotThrust());
		

		if(Keyboard.isKeyDown(Keyboard.KEY_I))
		{
			player.setRotation(0.0);
			player.setVelocityRot(0.0);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_J))
		{
			player.setRotation(270.0);
			player.setVelocityRot(0.0);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_K))
		{
			player.setRotation(180.0);
			player.setVelocityRot(0.0);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_L))
		{
			player.setRotation(90.0);
			player.setVelocityRot(0.0);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_O))
		{
			player.setVelocityRot(0.0);
			player.setVelocityX(0.0);
			player.setVelocityY(0.0);
		}
		
		while(Keyboard.next())
		{
			if(Keyboard.getEventKeyState())
			{
				
			}
		}
	}
}
