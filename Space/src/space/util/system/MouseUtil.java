package space.util.system;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class MouseUtil
{
	public static double getMouseX()
	{
		return Mouse.getX();
	}
	
	public static double getMouseY()
	{
		return (-Mouse.getY() + Display.getHeight() - 1);
	}
	
	public static double getMouseScroll()
	{
		return Mouse.getDWheel();
	}
}
