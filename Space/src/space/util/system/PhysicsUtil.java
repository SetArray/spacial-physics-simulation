package space.util.system;

import space.game.entity.IEntity;

public class PhysicsUtil
{
	public static double gravitational_constant = 6.67384e-11;
	public static final double metersPerPixel = 8000;

	public static double getGravitationalPull(IEntity entity1, IEntity entity2)
	{
		double F, G, M, m, R;

		F = 0;
		G = PhysicsUtil.gravitational_constant;
		M = entity1.getMass();
		m = entity2.getMass();
		R = Math.sqrt(Math.pow(MathUtil.getDifference(entity2.getGridX(), entity1.getGridX()), 2)
				+ Math.pow(MathUtil.getDifference(entity2.getGridY(), entity1.getGridY()), 2)) * metersPerPixel;
		F = ((G * (M * m) / Math.pow(R, 2))) / m;

		return F;
	}

	public static double calculateOrbitalVelocity(IEntity entity1, IEntity entity2, double distance)
	{
		double F = getGravitationalPull(entity1, entity2);
		
		return Math.sqrt((gravitational_constant * entity1.getMass()) / distance);
	}

	public static double getDistanceBetween(IEntity entity1, IEntity entity2)
	{
		return Math.sqrt(Math.pow(MathUtil.getDifference(entity2.getGridX(), entity1.getGridX()), 2)
				+ Math.pow(MathUtil.getDifference(entity2.getGridY(), entity1.getGridY()), 2)) * metersPerPixel;
	}
}
