package space.util.system;

public class MathUtil
{
	/**
	 * Round double to the nearest one-hundredth decimal place
	 * @param value
	 * @return
	 */
	public static double round(double value)
	{
		return Math.round(value * 100.0) / 100.0;
	}
	
	public static int roundUp(double value)
	{
		return (int) Math.ceil(value);
	}
	
	public static int roundDown(double value)
	{
		return (int) Math.floor(value);
	}
	
	public static double getDifference(Double a, Double b)
	{
		return Math.abs(a - b);
	}
}
